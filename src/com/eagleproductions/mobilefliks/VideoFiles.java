package com.eagleproductions.mobilefliks;


import java.util.ArrayList;


import android.app.ListActivity;

import android.os.Bundle;
import android.view.LayoutInflater;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class VideoFiles extends ListActivity{
       
	  ArrayList<DataCredentials>data=new ArrayList<DataCredentials>();
	  LayoutInflater linf;
	  DatabaseCls localdb;
    		  /** Called when the activity is first created. */
		  @Override
		  public void onCreate(Bundle savedInstanceState) {
		      super.onCreate(savedInstanceState);
		      this.linf = LayoutInflater.from(this);
		      localdb=new DatabaseCls(getApplicationContext());
		      localdb.open();
		      localdb.getPathDetails(this);
		      localdb.close();
		      
		  }
		  
		  
		  public static class MyViewHolder {
				public TextView name;
				public ImageView img;
				public DataCredentials blueprint;
				
			}
		  
	public void setData(ArrayList<DataCredentials> param){
		data=param;
		setListAdapter(new VideoListAdapter(this, linf, data));
	}
	
	public void displayResult(String message){
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
	}
}
