package com.eagleproductions.mobilefliks;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.Adapter.Item;

public class WalkThrough extends Activity {
	private Button skip;
	ArrayList<Item> dataItem;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_walkthrough);

		skip = (Button) findViewById(R.id.skipButton);

		// The onclick for the button
		skip.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(WalkThrough.this, Home.class));

			}
		});

		VideoView flikPlayer = (VideoView) findViewById(R.id.flikPlayerWalk);
		MediaController mediaController = new MediaController(this);
		mediaController.setAnchorView(flikPlayer);
		try {
			String vidpath = "android.resource://" + getPackageName() + "/"
					+ R.raw.test;
			flikPlayer.setVideoURI(Uri.parse(vidpath));
			// flikPlayer.setVideoPath(SrcPath);
			flikPlayer.setMediaController(new MediaController(this));
			flikPlayer.requestFocus();
			flikPlayer.start();
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), e.toString(),
					Toast.LENGTH_SHORT);
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}