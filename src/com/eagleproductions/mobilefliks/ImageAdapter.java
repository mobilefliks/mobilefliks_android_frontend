package com.eagleproductions.mobilefliks;

import java.util.ArrayList;
import java.util.List;

import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.Adapter.Item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {
	public List<Item> items = new ArrayList<Item>();
	private Context mContext;
	LayoutInflater inflater;
    ArrayList <Item>dataItems;
	public ImageAdapter(Context c,ArrayList<Item>p) {
		mContext = c;
		inflater = LayoutInflater.from(mContext);
		dataItems=p;
		 //populate list for adapter
		items.add(new Item("Midnight", "IE4X7Vg1PW", R.drawable.mid_night));
		items.add(new Item("Cracking Shells 2", "v0h0b2uAw6",
				R.drawable.cracking_shells));
		items.add(new Item("Cracking shells 1", "09XBRXLVtB",
				R.drawable.cracking_shells));
        
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// create a new ImageView for each item referenced by the Adapter
	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		View v = view;
		ImageView picture;
		TextView name;

		if (v == null) {
			v = inflater.inflate(R.layout.griditemlayout, viewGroup, false);
			v.setTag(R.id.picture, v.findViewById(R.id.picture));
			v.setTag(R.id.text, v.findViewById(R.id.text));
		}

		picture = (ImageView) v.getTag(R.id.picture);
		name = (TextView) v.getTag(R.id.text);

		Item item = (Item) getItem(i);

		//picture.setImageResource(item.drawableId);
		name.setText(item.name);

		return v;
	}

	
}