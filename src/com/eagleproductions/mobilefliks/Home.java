package com.eagleproductions.mobilefliks;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;

import com.eagleproductions.mobilefliks.R;
import com.eagleproductions.mobilefliks.Adapter.ImageLoadAdapter;
import com.eagleproductions.mobilefliks.Adapter.Item;
import com.eagleproductions.mobilefliks.Adapter.ItemLoader;
import com.eagleproductions.mobilefliks.Adapter.MgsImageLoader;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

/*import com.mpowerpayments.mpower.*;*/

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class Home extends Activity {
	ImageLoadAdapter mgrid;
	SharedPreferences mprefs;
	AdapterContextMenuInfo info;
	String pathFile, videaname;
	AlertDialog alert;
	int currentVersion;
	ArrayList<Item> dataItem;
	ArrayList<DataDetails> detaItem;
	LayoutInflater linf;
	GridView moviesGrid;
	GridView trendingMovies;
	ImageLoadAdapter trending;
	TextView tvLoading;
	Button Retry;
	// ArrayList<byte[]> img;
	byte[] img1;

	public static String url = "";
	Intent videos;
	private MgsImageLoader fetchImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//
		Parse.initialize(this, "X36WSd20k9qUS3K9QruKWVHIfP9lvUgjXv66sXl5",
				"ryVYLZkC6aVephck9OX5w0mV1gVOoZbPgZW0gTim");
		//

		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		mprefs = getSharedPreferences("Credentials", MODE_PRIVATE);
		String first = mprefs.getString("AfterFirst", "Default");
		String registration = mprefs.getString("email", "Default");
		if (first.equals("Default") || registration.equals("Default")) {
			SharedPreferences.Editor mEditor = mprefs.edit();
			mEditor.putString("AfterFirst", "true");
			mEditor.commit();
			startActivity(new Intent(this, RegisterUser.class));
		}

		tvLoading = (TextView) findViewById(R.id.tvLoading);
		Retry = (Button) findViewById(R.id.btnRetry);
		Retry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tvLoading.setVisibility(View.VISIBLE);
				Retry.setVisibility(View.INVISIBLE);
				loadParse();
			}
		});

		this.linf = LayoutInflater.from(this);
		videos = new Intent(Home.this, VideoFiles.class);
		this.fetchImage = new MgsImageLoader(this);
		dataItem = new ArrayList<Item>();

		Intent intent1 = new Intent(this, PayPalService.class);

		intent1.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_LIVE);
		intent1.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AX0kpxA_cQOHzvbMB3lLPyN_6Zeup4zrR9t4aUVfop4QnHC4fZjAYBIZsH7v");

		startService(intent1);
		moviesGrid = (GridView) findViewById(R.id.gridViewMovies);
		trendingMovies = (GridView) findViewById(R.id.gridViewTrending);
		// new ItemLoader(this).execute();
		loadParse();

		// the tab host section
		TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup();

		// adding the tab
		TabSpec addKickTab = tabHost.newTabSpec("tag1");
		addKickTab.setContent(R.id.addKicktab);
		addKickTab.setIndicator("TRENDING");
		tabHost.addTab(addKickTab);

		// adding the tab
		addKickTab = tabHost.newTabSpec("tag2");
		addKickTab.setContent(R.id.movies);
		addKickTab.setIndicator("MY MOVIES");
		tabHost.addTab(addKickTab);

		// trending contextual menu
		trendingMovies.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Item det = dataItem.get(position);
				Toast.makeText(Home.this, det.getName(), Toast.LENGTH_SHORT)
						.show();
			}
		});
		trendingMovies
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View v, int position, long id) {
						// TODO Auto-generated method stub
						Item det = dataItem.get(position);
						Toast.makeText(Home.this, det.getName(),
								Toast.LENGTH_SHORT).show();
						return false;
					}
				});

		moviesGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Item det = dataItem.get(position);
				Toast.makeText(Home.this, det.getName(), Toast.LENGTH_SHORT)
						.show();
			}
		});

		final Object[] data = (Object[]) getLastNonConfigurationInstance();
		if (data != null) {
			this.dataItem = (ArrayList<Item>) data[0];
			this.fetchImage = (MgsImageLoader) data[1];
			moviesGrid.setAdapter(new ImageLoadAdapter(this, this.linf,
					dataItem, fetchImage));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.splash, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
		switch (item.getItemId()) {
		case R.id.help:
			Intent intentHelp = new Intent(this, WalkThrough.class);
			startActivity(intentHelp);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// contextual menu onCreate
	public final static String EXTRA_MESSAGE = "url";

	public void purchaseItem(String VideoName, String currency) {
		PayPalPayment payment = new PayPalPayment(new BigDecimal("1.75"),
				"USD", videaname);

		Intent intent = new Intent(this, PaymentActivity.class);

		intent.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_LIVE);

		intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AX0kpxA_cQOHzvbMB3lLPyN_6Zeup4zrR9t4aUVfop4QnHC4fZjAYBIZsH7v");

		Log.d("Email", mprefs.getString("email", "Default"));
		intent.putExtra(PaymentActivity.EXTRA_PAYER_ID,
				mprefs.getString("email", "Default"));

		intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL,
				"ajoaatta@gmail.com");
		intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

		startActivityForResult(intent, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			PaymentConfirmation confirm = data
					.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
			if (confirm != null) {
				try {

					Log.i("paymentExample", confirm.toJSONObject().toString(4));
					createDialog("You are about to Download " + videaname
							+ "\nPress OK to Continue");
					displayResponse("Downloading in the background, will notify you when its done");
					currentVersion = android.os.Build.VERSION.SDK_INT;
					if (currentVersion < android.os.Build.VERSION_CODES.GINGERBREAD) {

					} else {
						new DownloadCls(this,
								Environment.getExternalStorageDirectory()
										+ "/Movies", videaname).execute(url);
					}

					// TODO: send 'confirm' to your server for verification.
					// see
					// https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
					// for more details.

				} catch (JSONException e) {
					Log.e("paymentExample",
							"an extremely unlikely failure occurred: ", e);

				}
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			Log.i("paymentExample", "The user canceled.");

		} else if (resultCode == PaymentActivity.RESULT_PAYMENT_INVALID) {
			Log.i("paymentExample",
					"An invalid payment was submitted. Please see the docs.");

		} else if (resultCode == 102) {
			String message = data.getStringExtra("Result");
			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
					.show();
			String vName = mprefs.getString(videaname, "Default");
			if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED)) {
				if (vName == "Default") {
					purchaseItem(videaname,
							mprefs.getString("currency", "Default"));
				} else {
					displayResponse("You have alredy downloaded this movie. Check the"
							+ Environment.getExternalStorageDirectory()
							+ "/Movies folder");
				}
			} else {
				displayResponse("No sdk card detected, please mount one and try again");
			}
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		stopService(new Intent(this, PayPalService.class));
		super.onDestroy();
	}

	public void displayResponse(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
				.show();
	}

	public void createDialog(String message) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
					}
				}).create();
		alert.show();

	}

	public void createPlayDialog(String message, final Item data) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						getpurch(data);
					}
				}).setNegativeButton("No", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
					}
				}).create();
		alert.show();

	}

	public void getpurch(Item data) {
		String credEmail = mprefs.getString("email", "Default");
		// String curr = mprefs.getString("currency", "Default");
		String vName = mprefs.getString(videaname, "Default");
		if (credEmail != "Default") {
			purchaseItem(videaname, "USD");

			ParseQuery<ParseObject> query = ParseQuery.getQuery("Movies");

			query.getInBackground(data.getUid(),
					new GetCallback<ParseObject>() {
						@Override
						public void done(ParseObject object, ParseException e) {
							if (e == null) {
								// object will be your game score

								ParseFile video = object.getParseFile("series");
								url = video.getUrl();

							} else {
								// something went wrong
								Toast.makeText(getApplicationContext(),
										"Something went wrong",
										Toast.LENGTH_LONG).show();
							}
						}
					});

			Intent intent = new Intent(this, PlayFlik.class);
			intent.putExtra(EXTRA_MESSAGE, url);
			startActivity(intent);

		} else {
			Intent regIntent = new Intent(this, RegisterUser.class);
			startActivityForResult(regIntent, 102);
		}
	}

	public void saveMovieCredentials(String name) {
		SharedPreferences.Editor meditor = mprefs.edit();
		meditor.putString(name, name);
		meditor.commit();
	}

	@Override
	@Deprecated
	public Object onRetainNonConfigurationInstance() {
		// TODO Auto-generated method stub
		Object[] mystuff = new Object[2];
		mystuff[0] = this.dataItem;
		mystuff[1] = this.fetchImage;
		return mystuff;
	}

	public static class MyViewHolder {
		public ImageView image;
		public TextView name;
		public Item data;

	}

	public void setImageIngrid(ArrayList<Item> param) {
		tvLoading.setVisibility(View.GONE);
		this.dataItem = param;
		// trending=new ImageLoadAdapter(this, linf, dataItem,this.fetchImage);
		trendingMovies.setAdapter(new ImageLoadAdapter(this, linf, dataItem,
				this.fetchImage));
		// moviesGrid.setAdapter(new ImageLoadAdapter(this, linf,
		// dataItem,this.fetchImage));
		registerForContextMenu(trendingMovies);
		// registerForContextMenu(moviesGrid);
	}

	public void loadParse() {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Movies");
		query.whereEqualTo("Trending", true);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> movieList, ParseException e) {
				if (e == null) {
					for (ParseObject movies : movieList) {
						ParseFile video = movies.getParseFile("series");
						url = video.getUrl();
						dataItem.add(new Item(movies.getString("title"), movies
								.getObjectId(), url, movies.getString("Price"),
								movies.getParseFile("images").getUrl(), movies
										.getString("gist")));
						Log.d(movies.getString("title"),
								movies.getString("gist"));
					}

				} else {
					System.out.println(e.getMessage());
				}
				/*
				 * displayResponse("The size of the arrayList " +
				 * String.valueOf(dataItem.size()));
				 */
				if (dataItem.size() > 0) {
					tvLoading.setVisibility(View.GONE);
					Retry.setVisibility(View.GONE);
					setImageIngrid(dataItem);
				} else {
					displayResponse(" No Connection ");
					tvLoading.setVisibility(View.GONE);
					Retry.setVisibility(View.VISIBLE);
				}
			}
		});
		Log.i("ArrayList Size",
				"the size of the array is" + String.valueOf(dataItem.size()));

	}

	public static class MyVHolder {
		public ImageView icon;
		public TextView name;
		public Item dataItem;
	}

	public void setimagebyte(byte[] img) {
		this.img1 = img;
	}

	public byte[] getimagebyte() {
		return this.img1;
	}

}
