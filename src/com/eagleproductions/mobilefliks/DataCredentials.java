package com.eagleproductions.mobilefliks;

public class DataCredentials {

	String videoname;
	String videopath;
	public DataCredentials(String videoname, String videopath) {
		super();
		this.videoname = videoname;
		this.videopath = videopath;
	}
	
	public String getVideoname() {
		return videoname;
	}
	public void setVideoname(String videoname) {
		this.videoname = videoname;
	}
	public String getVideopath() {
		return videopath;
	}
	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}
	
	
}
