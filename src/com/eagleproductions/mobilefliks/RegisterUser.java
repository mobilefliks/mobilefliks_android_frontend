package com.eagleproductions.mobilefliks;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.eagleproductions.mobilefliks.R;

public class RegisterUser extends Activity {
	EditText etEmail;
	//RadioGroup rgCurrency;
	Button register;

	SharedPreferences mprefs;

	//private int checked_item = -1;
	//private String checked_btn = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_user);
		mprefs = getSharedPreferences("Credentials", MODE_PRIVATE);
		etEmail = (EditText) findViewById(R.id.txtEmail);
		//rgCurrency = (RadioGroup) findViewById(R.id.Currencies);
		register = (Button) findViewById(R.id.btnRegister);

		//rgCurrency.setOnCheckedChangeListener(this);

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etEmail.getText().toString().length() > 0) {
					register();
				} else {
					Toast.makeText(
							getApplicationContext(),
							"Please Make sure you have provided every information",
							Toast.LENGTH_SHORT).show();
				}
			}

			private void register() {
				// TODO Auto-generated method stub
				
				SharedPreferences.Editor mEditor = mprefs.edit();
				mEditor.putString("email", etEmail.getText().toString());
				//mEditor.putString("currency", checked_btn);
				mEditor.commit();
				
				finish();
			}
		});
	}

	

}
