package com.eagleproductions.mobilefliks;

public class DataDetails {
     String pictureurl,videoname,price,videourl,description;

	public DataDetails(String pictureurl, String videoname, String price,
			String videourl, String description) {
		super();
		this.pictureurl = pictureurl;
		this.videoname = videoname;
		this.price = price;
		this.videourl = videourl;
		this.description = description;
	}

	public String getPictureurl() {
		return pictureurl;
	}

	public void setPictureurl(String pictureurl) {
		this.pictureurl = pictureurl;
	}

	public String getVideoname() {
		return videoname;
	}

	public void setVideoname(String videoname) {
		this.videoname = videoname;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
     
     
}
