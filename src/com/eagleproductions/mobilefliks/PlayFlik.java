package com.eagleproductions.mobilefliks;

import java.math.BigDecimal;

import org.json.JSONException;

import com.eagleproductions.mobilefliks.R;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class PlayFlik extends Activity {

	SharedPreferences mprefs;
	String url, price, name;
	AlertDialog alert;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.movieplayer);
		mprefs = getSharedPreferences("Credentials", MODE_PRIVATE);

		Intent intent1 = new Intent(this, PayPalService.class);

		intent1.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_LIVE);
		intent1.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AX0kpxA_cQOHzvbMB3lLPyN_6Zeup4zrR9t4aUVfop4QnHC4fZjAYBIZsH7v");

		startService(intent1);
		Bundle getGroup = getIntent().getExtras();
		this.price = getGroup.getString("Price");
		this.url = getGroup.getString("VideoUrl");
		this.name = getGroup.getString("VideoName");

		if (haveNetworkConnection()) {
			// this sets up a media player
			if (this.price.equals("free")) {

				VideoView flikPlayer = (VideoView) findViewById(R.id.flikPlayer);
				flikPlayer.setVideoURI(Uri.parse(url));
				flikPlayer.setMediaController(new MediaController(this));
				flikPlayer.requestFocus();
				flikPlayer.start();

				final ProgressDialog prog;
				prog = ProgressDialog.show(this, "Your flik will load soon...",
						name, true);

				flikPlayer.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						// TODO Auto-generated method stub
						prog.dismiss();
					}
				});

			} else {
				String credEmail = mprefs.getString("email", "Default");
				// String curr = mprefs.getString("currency", "Default");

				if (credEmail != "Default") {

					createPlayDialog("To watch this video, you will have to pay $"
							+ price + "\n Would you like to continue?");
				} else {
					Intent regIntent = new Intent(this, RegisterUser.class);
					startActivityForResult(regIntent, 102);
				}
			}

		} else {
			Toast.makeText(this,
					"You must be connected to the Internet to play",
					Toast.LENGTH_SHORT).show();
		}

	}

	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	public void purchaseItem(String VideoName, String currency) {
		PayPalPayment payment = new PayPalPayment(new BigDecimal(price),
				currency, VideoName);

		Intent intent = new Intent(this, PaymentActivity.class);

		intent.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT,
				PaymentActivity.ENVIRONMENT_LIVE);

		intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID,
				"AX0kpxA_cQOHzvbMB3lLPyN_6Zeup4zrR9t4aUVfop4QnHC4fZjAYBIZsH7v");

		Log.d("Email", mprefs.getString("email", "Default"));
		intent.putExtra(PaymentActivity.EXTRA_PAYER_ID,
				mprefs.getString("email", "Default"));

		intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL,
				"ajoaatta@gmail.com");
		intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

		startActivityForResult(intent, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == Activity.RESULT_OK) {
			PaymentConfirmation confirm = data
					.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
			if (confirm != null) {
				try {
					Log.i("paymentExample", confirm.toJSONObject().toString(4));

					VideoView flikPlayer = (VideoView) findViewById(R.id.flikPlayer);
					flikPlayer.setVideoURI(Uri.parse(url));
					flikPlayer.setMediaController(new MediaController(this));
					flikPlayer.requestFocus();
					flikPlayer.start();

					final ProgressDialog prog;
					prog = ProgressDialog.show(this,
							"Your flik will load soon...", name, true);

					flikPlayer.setOnPreparedListener(new OnPreparedListener() {
						@Override
						public void onPrepared(MediaPlayer mp) {
							// TODO Auto-generated method stub
							prog.dismiss();
						}
					});

				} catch (JSONException e) {
					Log.e("paymentExample",
							"an extremely unlikely failure occurred: ", e);
				}
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			Log.i("paymentExample", "The user canceled.");
			Toast.makeText(this, "Payment have been calcelled",
					Toast.LENGTH_SHORT).show();
			PlayFlik.this.finish();
		} else if (resultCode == PaymentActivity.RESULT_PAYMENT_INVALID) {
			Log.i("paymentExample",
					"An invalid payment was submitted. Please see the docs.");
		} else if (resultCode == 102) {
			String curr = mprefs.getString("currency", "Default");
			purchaseItem(name, curr);
		}
	}

	@Override
	public void onDestroy() {
		stopService(new Intent(this, PayPalService.class));
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	public void createDialog(String message) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
					}
				})
				.setNegativeButton("Cancel", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						PlayFlik.this.finish();
					}
				}).create();
		alert.show();

	}

	public void createPlayDialog(String message) {
		alert = new AlertDialog.Builder(this).setTitle("Information!")
				.setMessage(message)
				.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						purchaseItem(name, "USD");
					}
				}).setNegativeButton("No", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alert.cancel();
						PlayFlik.this.finish();

					}
				}).create();
		alert.show();

	}
}
